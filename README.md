# Advent of Code 2023 Python

Inspired by <https://realpython.com/python-advent-of-code/>

## Getting started

```shell
pip install poetry
poetry install
poetry run pytest
poetry run black .
poetry run mypy .
```

To run a specific day: `python src/aoc/day01.py input/01.txt`
