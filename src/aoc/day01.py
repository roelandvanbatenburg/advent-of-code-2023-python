import pathlib
import sys
import re


def parse(puzzle_input: str) -> list[int]:
    calibration_values = []
    for line in puzzle_input.split("\n"):
        first_digit = re.search(
            r"(\d|one|two|three|four|five|six|seven|eight|nine)", line
        )
        second_digit = re.search(
            r"^.*(\d|one|two|three|four|five|six|seven|eight|nine).*$", line
        )
        if first_digit and second_digit:
            calibration_values += [
                int(to_digit(first_digit.group(1)) + to_digit(second_digit.group(1)))
            ]

    return calibration_values


dict = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}


def to_digit(s):
    if s in dict:
        return dict[s]
    else:
        return s


def part1(data: list[int]) -> int:
    return sum(d for d in data)


def part2(data: list[int]) -> int:
    return sum(d for d in data)


if __name__ == "__main__":
    puzzle_input = pathlib.Path(sys.argv[1]).read_text().strip()
    data = parse(puzzle_input)
    print(part1(data))
    print(part2(data))
