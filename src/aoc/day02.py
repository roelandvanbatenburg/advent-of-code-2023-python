import pathlib
import sys
import re


def parse(puzzle_input: str) -> list[tuple[int, list[dict[str, int]]]]:
    games = []
    for line in puzzle_input.split("\n"):
        match = re.search(r"^Game (\d+): (.*)$", line)
        if match:
            games.append((int(match.group(1)), parse_rounds(match.group(2))))
    return games


def parse_rounds(rounds: str) -> list[dict[str, int]]:
    return [parse_round(round) for round in rounds.split("; ")]


def parse_round(round: str) -> dict[str, int]:
    return {item.split(" ")[1]: int(item.split(" ")[0]) for item in round.split(", ")}


def part1(games: list[tuple[int, list[dict[str, int]]]]) -> int:
    total = 0
    for game_id, rounds in games:
        if (
            max(rounds, "red") <= 12
            and max(rounds, "green") <= 13
            and max(rounds, "blue") <= 14
        ):
            total += game_id
    return total


def max(rounds, color):
    max = 0
    for round in rounds:
        if color in round and round[color] > max:
            max = round[color]
    return max


def part2(games: list[tuple[int, list[dict[str, int]]]]) -> int:
    total = 0
    for game_id, rounds in games:
        power = max(rounds, "red") * max(rounds, "green") * max(rounds, "blue")
        total += power
    return total


if __name__ == "__main__":
    puzzle_input = pathlib.Path(sys.argv[1]).read_text().strip()
    data = parse(puzzle_input)
    print(part1(data))
    print(part2(data))
