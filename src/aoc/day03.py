import pathlib
import sys


def parse(
    puzzle_input: str,
) -> tuple[dict[tuple[int, int, int], int], dict[tuple[int, int], str]]:
    lines = puzzle_input.splitlines()
    numbers = {}
    symbols = {}
    for y, line in enumerate(lines):
        number = (0, "")
        for x, char in enumerate(line):
            if char in "0123456789":
                if number[1] == "":
                    number = (x, char)
                else:
                    number = (number[0], number[1] + char)
            else:
                if number[1] != "":
                    numbers[(number[0], y, len(number[1]))] = int(number[1])
                    number = (0, "")
                if char != ".":
                    symbols[(x, y)] = char
        if number[1] != "":
            numbers[(number[0], y, len(number[1]))] = int(number[1])
    return (numbers, symbols)


def part1(
    data: tuple[dict[tuple[int, int, int], int], dict[tuple[int, int], str]]
) -> int:
    (numbers, symbols) = data
    sum = 0
    for (x, y, length), number in numbers.items():
        if has_symbol(x, y, length, symbols):
            sum += number
    return sum


def has_symbol(x_start, y_start, length, symbols):
    for x in range(x_start - 1, x_start + length + 1):
        for y in range(y_start - 1, y_start + 2):
            if (x, y) in symbols:
                return True
    return False


def part2(
    data: tuple[dict[tuple[int, int, int], int], dict[tuple[int, int], str]]
) -> int:
    (numbers, symbols) = data
    # lookup neighbors for gears
    number_locations = {}
    for (x, y, length), number in numbers.items():
        for i in range(length):
            number_locations[(x + i, y)] = number
    sum = 0
    for (x, y), symbol in symbols.items():
        if symbol == "*":
            neighbors = []
            for x_i in range(x - 1, x + 2):
                for y_i in range(y - 1, y + 2):
                    if (x_i, y_i) in number_locations:
                        number = number_locations[(x_i, y_i)]
                        if not number in neighbors:
                            neighbors.append(number)
            if len(neighbors) == 2:
                sum += neighbors[0] * neighbors[1]
    return sum


if __name__ == "__main__":
    puzzle_input = pathlib.Path(sys.argv[1]).read_text().strip()
    data = parse(puzzle_input)
    print(part1(data))
    print(part2(data))
