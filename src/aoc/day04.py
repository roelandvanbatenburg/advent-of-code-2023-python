import pathlib
import sys
import re


def parse(puzzle_input: str) -> list[tuple[list[int], list[int]]]:
    cards = []
    for line in puzzle_input.split("\n"):
        match = re.search(r"^Card.+(\d+): (.+) \| (.+)$", line)
        if match:
            cards.append(
                (
                    list(map(int, match.group(2).split())),
                    list(map(int, match.group(3).split())),
                )
            )
    return cards


def part1(data: list[tuple[list[int], list[int]]]) -> int:
    sum = 0
    for winning_numbers, numbers in data:
        number_of_wins = count_wins(winning_numbers, numbers)
        if number_of_wins > 0:
            sum += pow(2, number_of_wins - 1)
    return sum


def count_wins(winning_numbers, numbers):
    wins = list(filter(lambda number: (number in winning_numbers), numbers))
    return len(wins)


# 11024379
def part2(data: list[tuple[list[int], list[int]]]) -> int:
    card_numbers = [1 for card in data]
    for i, (winning_numbers, numbers) in enumerate(data):
        count = card_numbers[i]
        number_of_wins = count_wins(winning_numbers, numbers)
        for j in range(number_of_wins):
            card_index = i + j + 1
            if card_index >= len(card_numbers):
                break
            card_numbers[card_index] += count
    return sum(card_numbers)


if __name__ == "__main__":
    puzzle_input = pathlib.Path(sys.argv[1]).read_text().strip()
    data = parse(puzzle_input)
    print(part1(data))
    print(part2(data))
