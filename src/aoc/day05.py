import pathlib
import sys

Rule = list[tuple[int, int, int]]
Data = tuple[list[int], Rule, Rule, Rule, Rule, Rule, Rule, Rule]


def parse(puzzle_input: str) -> Data:
    lines = puzzle_input.split("\n\n")
    seeds = [int(seed) for seed in lines[0].split()[1:]]
    seed_to_soil = get_rules(lines[1])
    soil_to_fertilizer = get_rules(lines[2])
    fertilizer_to_water = get_rules(lines[3])
    water_to_light = get_rules(lines[4])
    light_to_temperature = get_rules(lines[5])
    temperature_to_humidity = get_rules(lines[6])
    humidity_to_location = get_rules(lines[7])
    return (
        seeds,
        seed_to_soil,
        soil_to_fertilizer,
        fertilizer_to_water,
        water_to_light,
        light_to_temperature,
        temperature_to_humidity,
        humidity_to_location,
    )


def get_rules(lines: str) -> list[tuple[int, int, int]]:
    rules = []
    for line in lines.split("\n")[1:]:
        [destination_start, source_start, steps] = [int(x) for x in line.split()]
        #             range_start,  range_end                  delta
        rules.append(
            (source_start, source_start + steps - 1, destination_start - source_start)
        )
    return sorted(rules)


def part1(data: Data) -> int:
    min = -1
    for seed in data[0]:
        for rules in data[1:]:
            seed = process_rules(seed, rules)
        if min < 0 or seed < min:
            min = seed
    return min


def process_rules(seed, rules):
    for start, end, delta in rules:
        if seed > end:
            continue
        if seed < start:
            return seed
        else:
            return seed + delta
    return seed


def part2(data: Data) -> int:
    seeds = data[0]
    mins = []
    for seed_index in range(0, len(seeds), 2):
        init_range = [(seeds[seed_index], seeds[seed_index] + seeds[seed_index + 1])]
        for rules in data[1:]:
            ranges = []
            for range_start, range_end in init_range:
                ranges += process_rules_for_range(range_start, range_end, rules)
            init_range = ranges
        mins.append(min(ranges)[0])
    return min(mins)


def process_rules_for_range(range_start, range_end, rules):
    ranges = []
    for start, end, delta in rules:
        if range_start > end or range_end < start:
            continue
        if range_start < start:
            ranges += [
                (range_start, start - 1),
                (start + delta, min(end, range_end) + delta),
            ]
        else:
            ranges += [(range_start + delta, min(end, range_end) + delta)]
        if end > range_end:
            return ranges
        range_start = end
    if not ranges:
        return [(range_start, range_end)]
    return ranges


if __name__ == "__main__":
    puzzle_input = pathlib.Path(sys.argv[1]).read_text().strip()
    data = parse(puzzle_input)
    print(part1(data))
    print(part2(data))
