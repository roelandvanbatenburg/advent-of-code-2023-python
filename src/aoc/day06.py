import pathlib
import sys


def parse(puzzle_input: str) -> tuple[list[str], list[str]]:
    lines = puzzle_input.split("\n")
    times = lines[0].split()[1:]
    distances = lines[1].split()[1:]
    return (times, distances)


def part1(data: tuple[list[str], list[str]]) -> int:
    (times, distances) = data
    product = 1
    for t, d in zip(times, distances):
        time = int(t)
        distance = int(d)
        options = 0
        for speed in range(time):
            travel = speed * (time - speed)
            if travel > distance:
                options += 1
        product *= options
    return product


def part2(data: tuple[list[str], list[str]]) -> int:
    (times, distances) = data
    time = int("".join(times))
    distance = int("".join(distances))
    options = 0
    for speed in range(time):
        travel = speed * (time - speed)
        if travel > distance:
            options += 1
    return options


if __name__ == "__main__":
    puzzle_input = pathlib.Path(sys.argv[1]).read_text().strip()
    data = parse(puzzle_input)
    print(part1(data))
    print(part2(data))
