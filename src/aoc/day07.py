import pathlib
import sys
from collections import Counter
import functools

Hand = tuple[str, int]


def parse(puzzle_input: str) -> list[Hand]:
    hands = []
    for line in puzzle_input.split("\n"):
        [cards, bid] = line.split()
        hands.append((cards, int(bid)))
    return hands


# score: 0=high card, 1=pair, 2=two pair, 3=three, 4=full, 5=four, 6=five
def hand_type1(cards):
    frequencies = Counter(list(cards))
    frequencies_frequencies = Counter(frequencies.values())
    max_frequency = max(frequencies_frequencies)

    if max_frequency == 5:
        return 6
    if max_frequency == 4:
        return 5
    if max_frequency == 1:
        return 0
    min_frequency = min(frequencies_frequencies)
    if max_frequency == 3 and min_frequency == 2:
        return 4
    if max_frequency == 3:
        return 3
    max_frequency_frequency = frequencies_frequencies[max_frequency]
    if max_frequency == 2 and max_frequency_frequency == 2:
        return 2
    return 1


def hand_type2(cards):
    frequencies = Counter(list(cards))
    jokers = 0
    if "J" in frequencies:
        jokers = frequencies["J"]
        frequencies.pop("J")
    frequencies_frequencies = Counter(frequencies.values())
    max_frequency = max(frequencies_frequencies) if frequencies_frequencies else 0

    if max_frequency + jokers == 5:
        return 6
    if max_frequency + jokers == 4:
        return 5
    if max_frequency + jokers == 1:
        return 0
    min_frequency = min(frequencies_frequencies)
    if (max_frequency + jokers == 3 and min_frequency == 2) or (
        max_frequency == 3 and min_frequency + jokers == 2
    ):
        return 4
    if max_frequency + jokers == 3:
        return 3
    # no need to account for jokers, they make three of a kind if they can
    max_frequency_frequency = frequencies_frequencies[max_frequency]
    if max_frequency == 2 and max_frequency_frequency == 2:
        return 2
    return 1


def card_height(card, joker_value):
    match card:
        case "A":
            return 14
        case "K":
            return 13
        case "Q":
            return 12
        case "J":
            return joker_value
        case "T":
            return 10
        case _:
            return int(card)


def compare1(hand1, hand2):
    (cards1, type1, _) = hand1
    (cards2, type2, _) = hand2
    if type1 > type2:
        return 1
    if type2 > type1:
        return -1
    for card1, card2 in zip(list(cards1), list(cards2)):
        if card1 == card2:
            continue
        if card_height(card1, 11) > card_height(card2, 11):
            return 1
        return -1
    return 0


def compare2(hand1, hand2):
    (cards1, type1, _) = hand1
    (cards2, type2, _) = hand2
    if type1 > type2:
        return 1
    if type2 > type1:
        return -1
    for card1, card2 in zip(list(cards1), list(cards2)):
        if card1 == card2:
            continue
        if card_height(card1, 1) > card_height(card2, 1):
            return 1
        return -1
    return 0


def part1(data: list[Hand]) -> int:
    hands = [(cards, hand_type1(cards), bid) for (cards, bid) in data]
    hands.sort(key=functools.cmp_to_key(compare1))
    sum = 0
    for i, (_, _, bid) in enumerate(hands):
        sum += (i + 1) * bid
    return sum


def part2(data: list[Hand]) -> int:
    hands = [(cards, hand_type2(cards), bid) for (cards, bid) in data]
    hands.sort(key=functools.cmp_to_key(compare2))
    sum = 0
    for i, (_, _, bid) in enumerate(hands):
        sum += (i + 1) * bid
    return sum


if __name__ == "__main__":
    puzzle_input = pathlib.Path(sys.argv[1]).read_text().strip()
    data = parse(puzzle_input)
    print(part1(data))
    print(part2(data))
