import pathlib
import sys
import re
import math

Network = dict[str, tuple[str, str]]


def parse(puzzle_input: str) -> tuple[str, Network]:
    instructions, _, *network_lines = puzzle_input.split("\n")
    network = {}
    for network_line in network_lines:
        match = re.search(r"^([A-Z]{3}) = \(([A-Z]{3}), ([A-Z]{3})\)$", network_line)
        if match:
            network[match.group(1)] = (match.group(2), match.group(3))
    return (instructions, network)


def part1(data: tuple[str, Network]) -> int:
    (instructions, network) = data
    i = 0
    steps = 0
    location = "AAA"
    while True:
        if i == len(instructions):
            i = 0
        steps += 1
        location = next_node(network, location, instructions[i])
        if location == "ZZZ":
            break
        i += 1
    return steps


def next_node(network, node, instruction):
    next = network[node]
    if instruction == "L":
        return next[0]
    else:
        return next[1]


def part2(data: tuple[str, Network]) -> int:
    (instructions, network) = data
    steps = []
    for node in network.keys():
        if not node.endswith("A"):
            continue
        i = 0
        step = 0
        while True:
            if i == len(instructions):
                i = 0
            step += 1
            node = next_node(network, node, instructions[i])
            if node.endswith("Z"):
                steps.append(step)
                break
            i += 1
    return math.lcm(*steps)


if __name__ == "__main__":
    puzzle_input = pathlib.Path(sys.argv[1]).read_text().strip()
    data = parse(puzzle_input)
    print(part1(data))
    print(part2(data))
