import pathlib
import sys


def parse(puzzle_input: str) -> list[list[int]]:
    sequences = []
    for line in puzzle_input.split("\n"):
        if line:
            sequences.append(list(map(int, line.split())))
    return sequences


def part1(data: list[list[int]]) -> int:
    return sum([extrapolate_right(sequence) for sequence in data])


def extrapolate_right(sequence):
    derivations = derive(sequence, [sequence])
    addition = 0
    for i in range(len(derivations) - 1, -1, -1):
        addition = derivations[i][-1] + addition
    return addition


def derive(sequence, derivations):
    derivation = []
    for i in range(len(sequence) - 1):
        derivation.append(sequence[i + 1] - sequence[i])
    derivations.append(derivation)
    if all(d == 0 for d in derivation):
        return derivations
    else:
        return derive(derivation, derivations)


def part2(data: list[list[int]]) -> int:
    return sum([extrapolate_left(sequence) for sequence in data])


def extrapolate_left(sequence):
    derivations = derive(sequence, [sequence])
    reduction = 0
    for i in range(len(derivations) - 1, -1, -1):
        reduction = derivations[i][0] - reduction
    return reduction


if __name__ == "__main__":
    puzzle_input = pathlib.Path(sys.argv[1]).read_text().strip()
    data = parse(puzzle_input)
    print(part1(data))
    print(part2(data))
