import pathlib
import sys
import math

Location = tuple[int, int]
Locations = dict[Location, str]
Data = tuple[Location, Locations]


def parse(puzzle_input: str) -> Data:
    lookup = {}
    for y, line in enumerate(puzzle_input.split("\n")):
        for x, char in enumerate(line):
            if char == "S":
                start = (x, y)
            lookup[(x, y)] = char

    return (start, lookup)


def part1(data: Data) -> int:
    (start, locations) = data
    previous = start
    current = find_connecting_location(start, locations)
    steps = 0
    while current != start:
        next = follow_connector(current, previous, locations[current])
        steps += 1
        previous = current
        current = next
    return math.ceil(steps / 2)


def find_connecting_location(start, locations):
    (x, y) = start
    if (x, y - 1) in locations and locations[(x, y - 1)] in ["F", "7", "|"]:
        return (x, y - 1)
    if (x, y + 1) in locations and locations[(x, y + 1)] in ["J", "L", "|"]:
        return (x, y + 1)
    if (x - 1, y) in locations and locations[(x - 1, y)] in ["F", "L", "-"]:
        return (x - 1, y)
    if (x + 1, y) in locations and locations[(x + 1, y)] in ["J", "7", "-"]:
        return (x + 1, y)
    raise "no connecting location found"


def follow_connector(location, previous_location, connector):
    (x_c, y_c) = location
    (x_p, y_p) = previous_location
    if connector == "|":
        if y_c > y_p:
            return (x_c, y_c + 1)
        return (x_c, y_c - 1)
    if connector == "-":
        if x_c > x_p:
            return (x_c + 1, y_c)
        return (x_c - 1, y_c)
    if connector == "J":
        if x_p < x_c:
            return (x_c, y_c - 1)
        return (x_c - 1, y_c)
    if connector == "7":
        if x_p < x_c:
            return (x_c, y_c + 1)
        return (x_c - 1, y_c)
    if connector == "F":
        if x_p > x_c:
            return (x_c, y_c + 1)
        return (x_c + 1, y_c)
    if connector == "L":
        if x_p > x_c:
            return (x_c, y_c - 1)
        return (x_c + 1, y_c)


def part2(data: Data) -> int:
    (start, locations) = data
    previous = start
    current = find_connecting_location(start, locations)
    pipes = [start, current]
    while current != start:
        next = follow_connector(current, previous, locations[current])
        pipes.append(next)
        previous = current
        current = next
    (size_x, size_y) = max(locations)
    in_loop = False
    prev_corner = ""
    enclosed_tiles = 0
    for y in range(size_y + 1):
        # print("y", y)
        for x in range(size_x + 1):
            symbol = locations[(x, y)]
            if (x, y) in pipes:
                match symbol:
                    case "S":
                        in_loop = not in_loop
                    case "|":
                        in_loop = not in_loop
                    case "L":
                        prev_corner = symbol
                    case "F":
                        prev_corner = symbol
                    case "J":
                        if prev_corner == "L":
                            continue
                        else:
                            in_loop = not in_loop
                    case "7":
                        if prev_corner == "F":
                            continue
                        else:
                            in_loop = not in_loop
            elif in_loop:
                enclosed_tiles += 1
    return enclosed_tiles


if __name__ == "__main__":
    puzzle_input = pathlib.Path(sys.argv[1]).read_text().strip()
    data = parse(puzzle_input)
    print(part1(data))
    print(part2(data))
