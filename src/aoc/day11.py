import pathlib
import sys

Coordinate = tuple[int, int]
Galaxies = dict[Coordinate, int]


def parse(puzzle_input: str) -> tuple[Galaxies, Coordinate]:
    galaxies = {}
    galaxy_id = 0
    max_y = 0
    max_x = 0
    for y, line in enumerate(puzzle_input.split("\n")):
        for x, char in enumerate(line):
            if char == "#":
                if y > max_y:
                    max_y = y
                if x > max_x:
                    max_x = x
                galaxies[(x, y)] = galaxy_id
                galaxy_id += 1
    return (galaxies, (max_x, max_y))


def part1(data: tuple[Galaxies, Coordinate]) -> int:
    galaxy_locations = expand_universe(data, 1)
    return total_distance(galaxy_locations)


def expand_universe(data, steps):
    (galaxies, (max_x, max_y)) = data
    right_shifts = []
    down_shifts = []
    for x in range(max_x, 0, -1):
        if not [True for y in range(max_y + 1) if (x, y) in galaxies]:
            right_shifts.append(x)
    for y in range(max_y, 0, -1):
        if not [True for x in range(max_x + 1) if (x, y) in galaxies]:
            down_shifts.append(y)
    for r in right_shifts:
        galaxies = shift_right(galaxies, r, steps)
    for d in down_shifts:
        galaxies = shift_down(galaxies, d, steps)
    galaxy_locations = {v: k for k, v in galaxies.items()}
    return galaxy_locations


def shift_right(galaxies, x, steps):
    new_galaxies = {}
    for x_galaxy, y_galaxy in galaxies:
        if x_galaxy > x:
            new_galaxies[(x_galaxy + steps, y_galaxy)] = galaxies[(x_galaxy, y_galaxy)]
        else:
            new_galaxies[(x_galaxy, y_galaxy)] = galaxies[(x_galaxy, y_galaxy)]
    return new_galaxies


def shift_down(galaxies, y, steps):
    new_galaxies = {}
    for x_galaxy, y_galaxy in galaxies:
        if y_galaxy > y:
            new_galaxies[(x_galaxy, y_galaxy + steps)] = galaxies[(x_galaxy, y_galaxy)]
        else:
            new_galaxies[(x_galaxy, y_galaxy)] = galaxies[(x_galaxy, y_galaxy)]
    return new_galaxies


def total_distance(galaxy_locations):
    number_of_galaxies = len(galaxy_locations)
    total_distance = 0
    for g1 in range(number_of_galaxies):
        for g2 in range(g1 + 1, number_of_galaxies):
            total_distance += distance(galaxy_locations[g1], galaxy_locations[g2])
    return total_distance


def distance(galaxy1, galaxy2):
    (x1, y1) = galaxy1
    (x2, y2) = galaxy2
    return abs(x1 - x2) + abs(y1 - y2)


def part2(data: tuple[Galaxies, Coordinate]) -> int:
    galaxy_locations = expand_universe(data, 1000000 - 1)
    return total_distance(galaxy_locations)


if __name__ == "__main__":
    puzzle_input = pathlib.Path(sys.argv[1]).read_text().strip()
    data = parse(puzzle_input)
    print(part1(data))
    print(part2(data))
