import pathlib
import sys

Rocks = list[list[int]]
RockLists = tuple[Rocks, Rocks]


def parse(puzzle_input: str) -> list[RockLists]:
    patterns = []
    for pattern in puzzle_input.split("\n\n"):
        patterns.append(parse_pattern(pattern))
    return patterns


def parse_pattern(pattern):
    rock_map = {}
    max_y = 0
    max_x = 0
    for y, line in enumerate(pattern.split("\n")):
        if y > max_y:
            max_y = y
        for x, char in enumerate(line):
            if x > max_y:
                max_x = x
            if char == "#":
                rock_map[(x, y)] = True

    y_rocks = []
    for y in range(max_y + 1):
        rocks = []
        for x in range(max_x + 1):
            if (x, y) in rock_map:
                rocks.append(x)
        y_rocks.append(rocks)

    x_rocks = []
    for x in range(max_x + 1):
        rocks = []
        for y in range(max_y + 1):
            if (x, y) in rock_map:
                rocks.append(y)
        x_rocks.append(rocks)
    return (y_rocks, x_rocks)


def part1(data: list[RockLists]) -> int:
    sum = 0
    for rock_data in data:
        (y_rocks, x_rocks) = rock_data
        v_reflection = get_reflection(x_rocks)
        if v_reflection:
            sum += v_reflection
        else:
            sum += 100 * get_reflection(y_rocks)
    return sum


def get_reflection(rocks):
    size = len(rocks)
    for i in range(1, size):
        rocks1 = rocks[:i]
        rocks2 = rocks[i:]
        l1 = len(rocks1)
        l2 = len(rocks2)
        if l1 > l2:
            rocks1 = rocks1[-l2:]
        elif l2 > l1:
            rocks2 = rocks2[:l1]
        rocks2.reverse()
        if rocks1 == rocks2:
            return i
    return 0


def part2(data: list[RockLists]) -> int:
    return 0


if __name__ == "__main__":
    puzzle_input = pathlib.Path(sys.argv[1]).read_text().strip()
    data = parse(puzzle_input)
    print(part1(data))
    print(part2(data))
