import pathlib
import sys


def parse(puzzle_input: str) -> list[str]:
    return puzzle_input.split(",")


def part1(data: list[str]) -> int:
    sum = 0
    for step in data:
        sum += hash(step)
    return sum


def hash(step):
    value = 0
    for char in step:
        char_value = ord(char)
        value += char_value
        value *= 17
        value = value % 256
    return value


def part2(data: list[str]) -> int:
    boxes: dict[int, list[str]] = {}
    labels_lookup: dict[str, int] = {}
    focus_lookup: dict[tuple[int, str], int] = {}
    for step in data:
        if "=" in step:
            [label, focus] = step.split("=")
            boxes, labels_lookup, focus_lookup = add_label(
                boxes, labels_lookup, focus_lookup, label, focus
            )
        if "-" in step:
            [label, _] = step.split("-")
            boxes, labels_lookup = remove_label(boxes, labels_lookup, label)
    focussing_power = 0
    for box in boxes:
        for slot, label in enumerate(boxes[box]):
            focussing_power += (box + 1) * (slot + 1) * int(focus_lookup[(box, label)])
    return focussing_power


def add_label(boxes, labels_lookup, focus_lookup, label, focus):
    box = hash(label)
    if box in boxes:
        if label in boxes[box]:
            i = boxes[box].index(label)
            boxes[box][i] = label
        else:
            boxes[box].append(label)
    else:
        boxes[box] = [label]
    focus_lookup[(box, label)] = focus
    labels_lookup[label] = box
    return boxes, labels_lookup, focus_lookup


def remove_label(boxes, labels_lookup, label):
    if label in labels_lookup:
        box = labels_lookup.pop(label)
        boxes[box].remove(label)

    return boxes, labels_lookup


if __name__ == "__main__":
    puzzle_input = pathlib.Path(sys.argv[1]).read_text().strip()
    data = parse(puzzle_input)
    print(part1(data))
    print(part2(data))
