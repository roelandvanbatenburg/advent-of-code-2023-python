import pathlib
import sys

# y, x
Coordinate = tuple[int, int]
Layout = dict[Coordinate, str]
Data = tuple[Layout, Coordinate]


def parse(puzzle_input: str) -> Data:
    layout = {}
    for y, line in enumerate(puzzle_input.split("\n")):
        for x, char in enumerate(line):
            layout[(y, x)] = char
    return (layout, (y, x))


def part1(data: Data, init=((0, 0), (0, 1))) -> int:
    (layout, (max_y, max_x)) = data
    next_locations_with_directions: list[tuple[Coordinate, Coordinate]] = [init]
    visited_locations_with_direction = {((0, 0), (0, 1)): init[0]}
    while next_locations_with_directions:
        (next_location, direction) = next_locations_with_directions.pop()

        match layout[next_location]:
            case ".":
                next = get_next(next_location, direction)
                (
                    next_locations_with_directions,
                    visited_locations_with_direction,
                ) = maybe_add_next(
                    next_locations_with_directions,
                    visited_locations_with_direction,
                    next,
                    max_y,
                    max_x,
                )
            case "|":
                if direction[0] == 1:
                    next = get_next(next_location, direction)
                    (
                        next_locations_with_directions,
                        visited_locations_with_direction,
                    ) = maybe_add_next(
                        next_locations_with_directions,
                        visited_locations_with_direction,
                        next,
                        max_y,
                        max_x,
                    )
                else:
                    up = get_next(next_location, (-1, 0))
                    (
                        next_locations_with_directions,
                        visited_locations_with_direction,
                    ) = maybe_add_next(
                        next_locations_with_directions,
                        visited_locations_with_direction,
                        up,
                        max_y,
                        max_x,
                    )
                    down = get_next(next_location, (1, 0))
                    (
                        next_locations_with_directions,
                        visited_locations_with_direction,
                    ) = maybe_add_next(
                        next_locations_with_directions,
                        visited_locations_with_direction,
                        down,
                        max_y,
                        max_x,
                    )
            case "-":
                if direction[1] == 1:
                    next = get_next(next_location, direction)
                    (
                        next_locations_with_directions,
                        visited_locations_with_direction,
                    ) = maybe_add_next(
                        next_locations_with_directions,
                        visited_locations_with_direction,
                        next,
                        max_y,
                        max_x,
                    )
                else:
                    left = get_next(next_location, (0, -1))
                    (
                        next_locations_with_directions,
                        visited_locations_with_direction,
                    ) = maybe_add_next(
                        next_locations_with_directions,
                        visited_locations_with_direction,
                        left,
                        max_y,
                        max_x,
                    )
                    right = get_next(next_location, (0, 1))
                    (
                        next_locations_with_directions,
                        visited_locations_with_direction,
                    ) = maybe_add_next(
                        next_locations_with_directions,
                        visited_locations_with_direction,
                        right,
                        max_y,
                        max_x,
                    )
            case "\\":
                direction = reflect_right(direction)
                next = get_next(next_location, direction)
                (
                    next_locations_with_directions,
                    visited_locations_with_direction,
                ) = maybe_add_next(
                    next_locations_with_directions,
                    visited_locations_with_direction,
                    next,
                    max_y,
                    max_x,
                )
            case "/":
                direction = reflect_left(direction)
                next = get_next(next_location, direction)
                (
                    next_locations_with_directions,
                    visited_locations_with_direction,
                ) = maybe_add_next(
                    next_locations_with_directions,
                    visited_locations_with_direction,
                    next,
                    max_y,
                    max_x,
                )
    energized = set(visited_locations_with_direction.values())
    return len(energized)


def get_next(location, direction):
    next_location = (location[0] + direction[0], location[1] + direction[1])
    return (next_location, direction)


def maybe_add_next(lst, mp, next, max_y, max_x):
    ((y, x), _) = next
    if y < 0 or y > max_y or x < 0 or x > max_x or next in mp:
        return lst, mp
    mp[next] = (y, x)
    lst.append(next)
    return lst, mp


def reflect_right(direction):
    (y, x) = direction
    return (x, y)


def reflect_left(direction):
    (y, x) = direction
    return (-x, -y)


def part2(data: Data) -> int:
    (layout, (max_y, max_x)) = data
    max_energized = 0
    for y in range(0, max_y + 1):
        energized = part1(data, ((y, 0), (0, 1)))
        if energized > max_energized:
            max_energized = energized
        energized = part1(data, ((y, max_x), (0, -1)))
        if energized > max_energized:
            max_energized = energized
    for x in range(0, max_x + 1):
        energized = part1(data, ((0, x), (1, 0)))
        if energized > max_energized:
            max_energized = energized
        energized = part1(data, ((max_y, x), (-1, 0)))
        if energized > max_energized:
            max_energized = energized
    return max_energized


if __name__ == "__main__":
    puzzle_input = pathlib.Path(sys.argv[1]).read_text().strip()
    data = parse(puzzle_input)
    print(part1(data))
    print(part2(data))
