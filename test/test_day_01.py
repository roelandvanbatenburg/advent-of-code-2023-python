import pytest
import aoc.day01 as aoc


@pytest.fixture
def example1():
    puzzle_input = """
1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet
    """.strip()
    return aoc.parse(puzzle_input)


@pytest.fixture
def example2():
    puzzle_input = """
two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen
""".strip()
    return aoc.parse(puzzle_input)


def test_parse_example1(example1):
    assert example1 == [12, 38, 15, 77]


def test_part1_example1(example1):
    assert aoc.part1(example1) == 142


def test_part2_example1(example2):
    assert aoc.part2(example2) == 281
