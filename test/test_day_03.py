import pytest
import aoc.day03 as aoc


@pytest.fixture
def example1():
    puzzle_input = """
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
    """.strip()
    return aoc.parse(puzzle_input)


def test_parse_example1(example1):
    assert example1 == (
        {
            (0, 0, 3): 467,
            (5, 0, 3): 114,
            (2, 2, 2): 35,
            (6, 2, 3): 633,
            (0, 4, 3): 617,
            (7, 5, 2): 58,
            (2, 6, 3): 592,
            (6, 7, 3): 755,
            (1, 9, 3): 664,
            (5, 9, 3): 598,
        },
        {(3, 1): "*", (6, 3): "#", (3, 4): "*", (5, 5): "+", (3, 8): "$", (5, 8): "*"},
    )


def test_part1_example1(example1):
    assert aoc.part1(example1) == 4361


def test_part2_example1(example1):
    assert aoc.part2(example1) == 467835
