import pytest
import aoc.day05 as aoc


@pytest.fixture
def example1():
    puzzle_input = """
seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
    """.strip()
    return aoc.parse(puzzle_input)


def test_parse_example1(example1):
    print(example1)
    (
        seeds,
        seed_to_soil,
        soil_to_fertilizer,
        fertilizer_to_water,
        water_to_light,
        light_to_temperature,
        temperature_to_humidity,
        humidity_to_location,
    ) = example1
    assert seeds == [79, 14, 55, 13]
    assert seed_to_soil == [(50, 97, 2), (98, 99, -48)]
    assert temperature_to_humidity == [(0, 68, 1), (69, 69, -69)]
    assert humidity_to_location == [(56, 92, 4), (93, 96, -37)]


def test_part1_example1(example1):
    assert aoc.part1(example1) == 35


def test_part2_example1(example1):
    assert aoc.part2(example1) == 46
