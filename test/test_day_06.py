import pytest
import aoc.day06 as aoc


@pytest.fixture
def example():
    puzzle_input = """
Time:      7  15   30
Distance:  9  40  200
    """.strip()
    return aoc.parse(puzzle_input)


def test_parse_example1(example):
    assert example == (["7", "15", "30"], ["9", "40", "200"])


def test_part1(example):
    assert aoc.part1(example) == 288


def test_part2(example):
    assert aoc.part2(example) == 71503
