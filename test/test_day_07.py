import pytest
import aoc.day07 as aoc


@pytest.fixture
def example():
    puzzle_input = """
32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483
    """.strip()
    return aoc.parse(puzzle_input)


def test_parse_example(example):
    assert example == [
        ("32T3K", 765),
        ("T55J5", 684),
        ("KK677", 28),
        ("KTJJT", 220),
        ("QQQJA", 483),
    ]


def test_part1(example):
    assert aoc.part1(example) == 6440


def test_part2(example):
    assert aoc.part2(example) == 5905
