import pytest
import aoc.day08 as aoc


@pytest.fixture
def example():
    puzzle_input = """
RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)
    """.strip()
    return aoc.parse(puzzle_input)


@pytest.fixture
def example2():
    puzzle_input = """
LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)
    """.strip()
    return aoc.parse(puzzle_input)


@pytest.fixture
def example3():
    puzzle_input = """
LR

FFA = (FFB, XXX)
FFB = (XXX, FFZ)
FFZ = (FFB, XXX)
GGA = (GGB, XXX)
GGB = (GGC, GGC)
GGC = (GGZ, GGZ)
GGZ = (GGB, GGB)
XXX = (XXX, XXX)
    """.strip()
    return aoc.parse(puzzle_input)


def test_parse_example(example):
    assert example == (
        "RL",
        {
            "AAA": ("BBB", "CCC"),
            "BBB": ("DDD", "EEE"),
            "CCC": ("ZZZ", "GGG"),
            "DDD": ("DDD", "DDD"),
            "EEE": ("EEE", "EEE"),
            "GGG": ("GGG", "GGG"),
            "ZZZ": ("ZZZ", "ZZZ"),
        },
    )


def test_part1(example):
    assert aoc.part1(example) == 2


def test_part1_2(example2):
    assert aoc.part1(example2) == 6


def test_part2(example3):
    assert aoc.part2(example3) == 6
