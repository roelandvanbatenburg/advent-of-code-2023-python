import pytest
import aoc.day09 as aoc


@pytest.fixture
def example():
    puzzle_input = """
0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45
    """.strip()
    return aoc.parse(puzzle_input)


def test_parse_example(example):
    assert example == [
        [0, 3, 6, 9, 12, 15],
        [1, 3, 6, 10, 15, 21],
        [10, 13, 16, 21, 30, 45],
    ]


def test_part1(example):
    assert aoc.part1(example) == 114


def test_part2(example):
    assert aoc.part2(example) == 2
