import pytest
import aoc.day10 as aoc


@pytest.fixture
def example():
    puzzle_input = """
..F7.
.FJ|.
SJ.L7
|F--J
LJ...
    """.strip()
    return aoc.parse(puzzle_input)


@pytest.fixture
def example2():
    puzzle_input = """
...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........
    """.strip()
    return aoc.parse(puzzle_input)


@pytest.fixture
def example3():
    puzzle_input = """
.F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...
    """.strip()
    return aoc.parse(puzzle_input)


def test_parse_example(example):
    (start, lookup) = example
    assert start == (0, 2)
    assert lookup[(4, 2)] == "7"


def test_part1(example):
    assert aoc.part1(example) == 8


def test_part2(example2):
    assert aoc.part2(example2) == 4


def test_part2_3(example3):
    assert aoc.part2(example3) == 8
