import pytest
import aoc.day11 as aoc


@pytest.fixture
def example():
    puzzle_input = """
...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....
    """.strip()
    return aoc.parse(puzzle_input)


def test_parse_example(example):
    assert example == (
        {
            (3, 0): 0,
            (7, 1): 1,
            (0, 2): 2,
            (6, 4): 3,
            (1, 5): 4,
            (9, 6): 5,
            (7, 8): 6,
            (0, 9): 7,
            (4, 9): 8,
        },
        (9, 9),
    )


def test_part1(example):
    assert aoc.part1(example) == 374


def test_part2(example):
    assert aoc.part2(example) == 82000210
