import pytest
import aoc.day15 as aoc


@pytest.fixture
def example():
    puzzle_input = """
rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7
    """.strip()
    return aoc.parse(puzzle_input)


def test_parse_example(example):
    assert example == [
        "rn=1",
        "cm-",
        "qp=3",
        "cm=2",
        "qp-",
        "pc=4",
        "ot=9",
        "ab=5",
        "pc-",
        "pc=6",
        "ot=7",
    ]


def test_part1(example):
    assert aoc.part1(example) == 1320


def test_part2(example):
    assert aoc.part2(example) == 145
