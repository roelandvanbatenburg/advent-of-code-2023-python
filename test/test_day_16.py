import pytest
import aoc.day16 as aoc


@pytest.fixture
def example():
    puzzle_input = """
.|...\\....
|.-.\\.....
.....|-...
........|.
..........
.........\\
..../.\\\\..
.-.-/..|..
.|....-|.\\
..//.|....
    """.strip()
    return aoc.parse(puzzle_input)


def test_parse_example(example):
    (layout, (max_y, max_x)) = example
    assert layout[0, 0] == "."
    assert layout[0, 1] == "|"
    assert layout[0, 5] == "\\"
    assert layout[9, 3] == "/"
    assert max_y == 9
    assert max_x == 9


def test_part1(example):
    assert aoc.part1(example) == 46


def test_part2(example):
    assert aoc.part2(example) == 51
